INCLUDEPATH += $$PWD
DEPENDPATH += $$PWD
SOURCES += $$PWD/QVirtualKeyboard.cpp \
    $$PWD//numericalkeyboard.cpp \
    $$PWD//alphabeticalkeyboard.cpp \
    $$PWD//qpushbutton1.cpp

HEADERS += $$PWD/QVirtualKeyboard.h \
    $$PWD//numericalkeyboard.h \
    $$PWD//alphabeticalkeyboard.h \
    $$PWD//qpushbutton1.h \
    widgets/KeyboardWidget/keyboardstylessheets.h
