QT       += core gui svg multimedia

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets printsupport

TARGET = ui
TEMPLATE = app

include(gui/gui.pri)
include(model/model.pri)
include(common/common.pri)
include(svgdialgauge/svgdialgauge.pri)
include(svgbutton/svgbutton.pri)
include(svgslideswitch/svgslideswitch.pri)
include(KeyboardWidget/KeyboardWidget.pri)
include(PasswordWidget/PasswordWidget.pri)

SOURCES += main.cpp\
        mainwindow.cpp

HEADERS  += mainwindow.h

FORMS    += mainwindow.ui

RESOURCES += resources/resources.qrc

QMAKE_CXXFLAGS += -Wno-psabi   # disable "mangling of 'va_list' has changed warning
QMAKE_CXXFLAGS += -O3 -Wall -Wextra -std=c++11 -pedantic -Werror
QMAKE_CXXFLAGS += -Wno-error=strict-overflow # strict overflow occurs in some Qt headers
